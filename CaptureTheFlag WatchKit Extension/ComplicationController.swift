//
//  ComplicationController.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 15/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import ClockKit


class ComplicationController: NSObject, CLKComplicationDataSource {
    
    // MARK: Register
    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler:
        @escaping (CLKComplicationTemplate?) -> Swift.Void) {
        
// 1
        if complication.family == .graphicCircular { // 2
        let smallFlat = CLKComplicationTemplateGraphicCircularImage() // 3
            //        smallFlat.textProvider = CLKSimpleTextProvider(text: "+2.6m") // 4
        smallFlat.imageProvider = CLKFullColorImageProvider( fullColorImage: UIImage(named: "Elerium")!)
            // 5
        handler(smallFlat)
        }
        else {
            handler(nil)
        }
//        if complication.family == .utilitarianLarge{
//
//        let largeFlat = CLKComplicationTemplateUtilitarianLargeFlat()
//            largeFlat.textProvider = CLKSimpleTextProvider(text: "Rising, +2.6m", shortText:"+2.6m")
//            largeFlat.imageProvider = CLKImageProvider(onePieceImage: UIImage(named: "Calliope")!)
//            handler(largeFlat)
//            }
    }
    
    // MARK: Provide Data
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Swift.Void) { handler(nil)
    }
    // MARK: Time Travel
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Swift.Void) { handler([])
    }
    
}
