//
//  SwiftUIView.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 20/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import SwiftUI

struct ClanChoiceContentView: View {
    
    @State var isFirstTime: Bool = true
    
    var body: some View {
        
        VStack {
            Text("Choose your clan!")
            HStack {
                NavigationLink(destination: MapContentView()) {
                    Image(uiImage: #imageLiteral(resourceName: "Grande.png"))
                        .accentColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                }
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                
                Button(action: {self.isFirstTime.toggle()}) {
                    Image(uiImage: #imageLiteral(resourceName: "Grande.png"))
                        .accentColor(.red)
                }
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            }
            Spacer()
            HStack {
                Button(action: {self.isFirstTime.toggle()}) {
                    Image(uiImage: #imageLiteral(resourceName: "Grande.png"))
                        .accentColor(.yellow)
                }
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                Button(action: {self.isFirstTime.toggle()}) {
                    Image(uiImage: #imageLiteral(resourceName: "Grande.png"))
                        .accentColor(.white)
                }
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            }
        }
    }
}

struct ClanChoiceContentView_Previews: PreviewProvider {
    static var previews: some View {
        ClanChoiceContentView()
    }
}
