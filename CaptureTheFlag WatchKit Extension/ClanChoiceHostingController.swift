//
//  ClanChoiceHostingController.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 20/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class ClanChoiceHostingController: WKHostingController<ClanChoiceContentView> {
    override var body: ClanChoiceContentView {
        return ClanChoiceContentView()
    }
}
