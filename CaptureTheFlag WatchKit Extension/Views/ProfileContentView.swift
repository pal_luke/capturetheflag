//
//  SwiftUIView.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 20/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import SwiftUI


struct Gamer: Identifiable {
    var id: UUID = UUID() /// To identify elements univocally
    var name: String
    var role: String
    var color: Color
    
    init(name: String, role: String, color: Color) {
        self.name = name
        self.role = role
        self.color = color
    }
}

// CUSTOM SWITCH
struct ColoredToggleStyle: ToggleStyle {
    var label = ""
    var onColor = Color(UIColor.green)
    var offColor = Color(UIColor.gray)
    var thumbColor = Color.white

    func makeBody(configuration: Self.Configuration) -> some View {
        HStack {
            Text(label)
            Spacer()
            Button(action: { configuration.isOn.toggle() } )
            {
                RoundedRectangle(cornerRadius: 16, style: .circular)
                    .fill(configuration.isOn ? onColor : offColor)
                    .frame(width: 34, height: 22)
                    .overlay(
                        Circle()
                            .fill(thumbColor)
                            .shadow(radius: 5, x: 0, y: 1)
//                            .padding(1.5)
                            .offset(x: configuration.isOn ? 10 : -10))
                    .animation(Animation.easeInOut(duration: 0.1))
            }
            
            .accentColor(.black)
        }
        .font(.title)
        .padding(.horizontal)
    }
}

// CUSTOM BUTTON
struct CustomButton: View {
    
    var name: String
    var role: String
    var color: Color
    
    var body: some View {
        Button(action: {}) {
            VStack {
                Text(self.name)
                if self.role != "" {
                    Text(self.role)
                        .font(.footnote)
                        .colorInvert()
                }
            }
            .padding(6.0)
        }
        .background(self.color)
        .cornerRadius(8)
    }
    
    init(gamer: Gamer) {
        self.name = gamer.name
        self.role = gamer.role
        self.color = gamer.color
    }
}


struct ProfileContentView: View {
    @State var isFirstTime = true
    @State var isChecked = true
    @State var clan = Color.orange

    var body: some View {
        return Group {
            if isFirstTime && setFirstTime() {
                ClanChoiceView(isFirstTime: $isFirstTime, clan: $clan)
            } else if !isChecked {
                NotVisibleContent(isChecked: $isChecked, clan: $clan)
            }
            else {
                ProfileContent(isChecked: $isChecked, clan: $clan)
            }
        }
    }
    
    func setFirstTime() -> Bool {
        UserDefaults.standard.set(true, forKey: "isFirstTime")
        UserDefaults.standard.set(false, forKey: "isMapVisible")
        return true
    }
}


struct ClanChoiceView : View {

    @Binding var isFirstTime: Bool
    @State var isMapVisible: Bool = false
    @Binding var clan: Color

    var body: some View {
        
        VStack {
            Text("Choose your clan!")
                .font(.headline)
            HStack {
                Button(action: {
                    self.isFirstTime.toggle()
                    self.isMapVisible.toggle()
                    self.clan = Color.blue
                    UserDefaults.standard.set(self.isFirstTime, forKey: "isFirstTime")
                    UserDefaults.standard.set(self.isMapVisible, forKey: "isMapVisible")
                }) {
                    Image("water")
                        .accentColor(.blue)
                }
                .background(Color.black)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                
                Button(action: {
                    self.isFirstTime.toggle()
                    self.isMapVisible.toggle()
                    self.clan = Color.red
                    UserDefaults.standard.set(self.isFirstTime, forKey: "isFirstTime")
                    UserDefaults.standard.set(self.isMapVisible, forKey: "isMapVisible")
                }) {
                    Image("fire")
                        .accentColor(.red)
                }
                .background(Color.black)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            }
            
            Spacer()
            
            HStack {
                Button(action: {
                    self.isFirstTime.toggle()
                    self.isMapVisible.toggle()
                    self.clan = Color(#colorLiteral(red: 0, green: 0.6997902989, blue: 0.2542638779, alpha: 1))
                    UserDefaults.standard.set(self.isFirstTime, forKey: "isFirstTime")
                    UserDefaults.standard.set(self.isMapVisible, forKey: "isMapVisible")
                }) {
                    Image("earth")
                        .accentColor(.green)
                }
                .background(Color.black)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                
                Button(action: {
                    self.isFirstTime.toggle()
                    self.isMapVisible.toggle()
                    self.clan = Color(#colorLiteral(red: 0.5882352941, green: 0.5529411765, blue: 0.9411764706, alpha: 1))
                    UserDefaults.standard.set(self.isFirstTime, forKey: "isFirstTime")
                    UserDefaults.standard.set(self.isMapVisible, forKey: "isMapVisible")
                }) {
                    Image("air")
                        .accentColor(Color(#colorLiteral(red: 0.5882352941, green: 0.5529411765, blue: 0.9411764706, alpha: 1)))
                }
                .background(Color.black)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            }
        }
    }
}


struct ProfileContent: View {
    
    @State private var showRedStroke = false
    @State private var showGreenStroke = false
    @State private var showBlueStroke = false
    @State private var showModal: Bool = false
    @Binding var isChecked: Bool
    @Binding var clan: Color

    var body: some View {
        ScrollView {
            VStack {
                Spacer()
                ZStack {
                    RadialGradient(gradient: Gradient(colors: [clan, Color.black]), center: .center, startRadius: 10, endRadius: 70)
                        .scaleEffect(1.0)
                    
                    // RED Stroke
                    Circle()
                        .trim(from: showRedStroke ? 1/8 : 1, to: 1)
                        .stroke(style: StrokeStyle(lineWidth: 15, lineCap: .round, lineJoin: .round))
                        .frame(width: 125, height: 125)
                        .foregroundColor(clan)
                        .rotationEffect(.degrees(90)) // Flips the strokeStart
                        .rotation3DEffect(.degrees(180), axis: (x:1, y:0, z:0))
                        .shadow(color: .black, radius: 20, y: 5)
                        .animation(Animation.easeIn(duration: 1.0))
                        .onAppear() {
                            self.showRedStroke = true
                    }
                    .onDisappear() {
                        self.showRedStroke.toggle()
                    }
                    
                    // GREEN Stroke
//                    Circle()
//                        .trim(from: showGreenStroke ? 1/3 : 1, to: 1)
//                        .stroke(style: StrokeStyle(lineWidth: 15, lineCap: .round, lineJoin: .round))
//                        .frame(width: 94.5, height: 94.5)
//                        .foregroundColor(Color(red: 0.618, green: 0.968, blue: -0.013))
//                        .rotationEffect(.degrees(90)) // Flips the strokeStart
//                        .rotation3DEffect(.degrees(180), axis: (x:1, y:0, z:0))
//                        .shadow(color: .black, radius: 20, y: 5)
//                        .animation(Animation.easeOut(duration: 1))
//                        .onAppear() {
//                            self.showGreenStroke = true
//                    }
//                    .onDisappear() {
//                        self.showGreenStroke.toggle()
//                    }
                    
                    // BLUE Stroke
//                    Circle()
//                        .trim(from: showBlueStroke ? 1/6 : 1, to: 1)
//                        .stroke(style: StrokeStyle(lineWidth: 15, lineCap: .round, lineJoin: .round))
//                        .frame(width: 64, height: 64)
//                        .foregroundColor(Color(red: 0.002, green: 0.884, blue: 0.842))
//                        .rotationEffect(.degrees(90)) // Flips the strokeStart
//                        .rotation3DEffect(.degrees(180), axis: (x:1, y:0, z:0))
//                        .shadow(color: .black, radius: 20, y: 5)
//                        .animation(Animation.easeOut(duration: 1))
//                        .onAppear() {
//                            self.showBlueStroke = true
//                    }
//                    .onDisappear() {
//                        self.showBlueStroke.toggle()
//                    }
                    
                    Button(action: {
                        self.showModal = true
                    }) {
                        if clan == Color.blue {
                            Image("water")
                                .accentColor(clan)
                        } else if clan == Color.red {
                            Image("fire")
                                .accentColor(clan)
                        } else if clan == Color(#colorLiteral(red: 0, green: 0.6997902989, blue: 0.2542638779, alpha: 1)) {
                            Image("earth")
                                .accentColor(.green)
                        } else if clan == Color(#colorLiteral(red: 0.5882352941, green: 0.5529411765, blue: 0.9411764706, alpha: 1)) {
                            Image("air")
                                .accentColor(clan)
                        }
                    }
                    .background(Color.black)
                    .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    .sheet(isPresented: self.$showModal){
                        FriendsListModalView(clan: self.$clan)
                    }
                }
                Text("Level 15")
                Spacer()
                HStack {
                    Text("Visibility")
                    Spacer()
                    Toggle(isOn: $isChecked) {
                        if(!UserDefaults.standard.bool(forKey: "isMapVisible")) {
                            Text(toggleAction())
                        }
                    }
                }
                .toggleStyle(
                ColoredToggleStyle( label:"    ",
                               onColor: clan,
                               offColor: .white,
                               thumbColor: Color(UIColor.white)))
                .padding(.horizontal)
            }
        }
        .navigationBarTitle("Profile")
    }
    
    func toggleAction() -> String {
        UserDefaults.standard.set(true, forKey: "isMapVisible")
        print("STATE: \(UserDefaults.standard.bool(forKey: "isMapVisible"))")
        return ""
    }
}


struct NotVisibleContent: View {
    
    @Binding var isChecked: Bool
    @Binding var clan: Color
    
    var body: some View {
        
        VStack {
            if clan == Color.blue {
                Image("waterEye")
            } else if clan == Color.red {
                Image("fireEye")
            } else if clan == Color(#colorLiteral(red: 0, green: 0.6997902989, blue: 0.2542638779, alpha: 1)) {
                Image("earthEye")
            } else if clan == Color(#colorLiteral(red: 0.5882352941, green: 0.5529411765, blue: 0.9411764706, alpha: 1)) {
                Image("airEye")
            }
            
            HStack {
                Text("Visibility")
                Spacer()
                Toggle(isOn: $isChecked) {
                    Text(toggleAction())        /// Evokes the function "toggleAction" and prints nothing, it's just a turn around
                }
                .toggleStyle(
                ColoredToggleStyle( label:"    ",
                               onColor: clan,
                               offColor: .white,
                               thumbColor: Color(UIColor.white)))
                .padding(.horizontal)
            }
        }
        .padding(.horizontal)
    }
    
    func toggleAction() -> String {
        UserDefaults.standard.set(false, forKey: "isMapVisible")
        print("STATENOT: \(UserDefaults.standard.bool(forKey: "isMapVisible"))")
        return ""
    }
}


struct FriendsListModalView: View {
    
    @Binding var clan: Color
    
    func createGamersList(clan: Color) -> [Gamer] {
        return [Gamer(name: "Stasia Kang", role: "Squad Leader", color: clan), Gamer(name: "Tammie Yankey", role: "Current Squad", color: clan), Gamer(name: "Waylon Toppin", role: "Current Squad", color: clan), Gamer(name: "Simone Wasko", role: "Current Squad", color: clan), Gamer(name: "Austin Prater", role: "", color: Color.clear), Gamer(name: "Tiera Brooking", role: "", color: Color.clear), Gamer(name: "Hunter Ricca", role: "", color: Color.clear), Gamer(name: "Billi Majewski", role: "", color: Color.clear)]
    }
    
    
    var body: some View {
        VStack(alignment: .leading) {
            List {
                ForEach (createGamersList(clan: clan)) { element in
                    CustomButton(gamer: element)
                }
            }
        }
    }
}


struct ProfileContentView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileContentView()
    }
}
