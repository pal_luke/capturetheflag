//
//  FlagsContentView.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 22/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import SwiftUI

struct FlagsContentView: View {
    
    var body: some View {
        VStack {
            if UserDefaults.standard.bool(forKey: "isMapVisible") == true {
                Spacer()
                HStack {
                    ButtonDistance(color: Color.red, distance: "100 m")
                    ButtonDistance(color: Color.blue, distance: "230 m")
                }
                Spacer()
                HStack {
                    ButtonDistance(color: Color(#colorLiteral(red: 0, green: 0.6997902989, blue: 0.2542638779, alpha: 1)), distance: "1 km")
                    ButtonDistance(color: Color(#colorLiteral(red: 0.5882352941, green: 0.5529411765, blue: 0.9411764706, alpha: 1)), distance: "2 km")
                }
            } else {
                Text("Visibility is Off")
                Text("Please switch it on")
                    .font(.footnote)
            }
        }
        .navigationBarTitle("Flags")
    }
}


struct ButtonDistance: View {
    var color: Color
    var distance: String
    var body: some View {
        VStack {
            FlagButton(color: color, image: Image(systemName: "flag.fill"))
            Text(distance)
        }
    }
    
    init(color: Color, distance: String) {
        self.color = color
        self.distance = distance
    }
    
}

struct FlagButton: View {
    @State private var showModal: Bool = false
    var color: Color
    var image: Image
    var body: some View {
        Button(action: {self.showModal = true}) {
            Text("")
        }
        .background(image)
        .background(color)
        .clipShape(Circle())
        .sheet(isPresented: self.$showModal){
            ModalView()
        }
    }
    
    init(color: Color, image: Image) {
        self.color = color
        self.image = image
    }
}

struct Question: View {
    var question: String
    var correctAns: String
    var wrongAns: String
    var randomBoolean = Bool.random()
    @State private var correct: Color = .black
    @State private var wrong: Color = .black
    @State private var disableC = false
    @State private var disableW = false
    
    var body: some View {
        VStack(alignment: .leading) {
            Spacer()
            Text(question)
                .font(.headline)
            Spacer()
            
            if(randomBoolean) {
                HStack {
                    Button(action: {
                        self.correct = .green
                        self.disableW = true
                    }) {
                        Text(correctAns)
                    }
                    .background(correct)
                    .disabled(disableC)
                    .cornerRadius(20)
                    
                    Button(action: {
                        self.wrong = .red
                        self.disableC = true
                    }) {
                        Text(wrongAns)
                    }
                    .background(wrong)
                    .disabled(disableW)
                    .cornerRadius(20)
                }
            }
            else {
                HStack {
                    Button(action: {
                        self.wrong = .red
                        self.disableC = true
                    }) {
                        Text(wrongAns)
                    }
                    .background(wrong)
                    .disabled(disableW)
                    .cornerRadius(20)
                    
                    Button(action: {
                        self.correct = .green
                        self.disableW = true
                    }) {
                        Text(correctAns)
                    }
                    .background(correct)
                    .disabled(disableC)
                    .cornerRadius(20)
                }
            }
        }
    }
    
    init(question: String, correctAns: String, wrongAns: String) {
        self.question = question
        self.correctAns = correctAns
        self.wrongAns = wrongAns
    }
}

let QuestionList: [Question] = [
    Question(question: "Do you like our app?", correctAns: "Yes", wrongAns: "No"),
    Question(question: "Who is the best virtual assistant?", correctAns: "Siri", wrongAns: "Alexa"),
    Question(question: "A goldfish has a memory of only 3 seconds.", correctAns: "False", wrongAns: "True"),
    Question(question: "Babies have more bones than adults.", correctAns: "True", wrongAns: "False"),
    Question(question: "When a duck quacks it doesn't echo.", correctAns: "True", wrongAns: "False"),
    Question(question: "0 is an even and an odd number.", correctAns: "False", wrongAns: "True"),
    Question(question: "Is odontophobia the fear of theet?", correctAns: "Yes", wrongAns: "No"),
    Question(question: "What year was the very first model of the iPhone released?", correctAns: "2007", wrongAns: "2008")
]

struct ModalView: View {
    var body: some View {
            VStack(alignment: .leading) {
                QuestionList.randomElement()
            }
    }
}

struct FlagsContentView_Previews: PreviewProvider {
    static var previews: some View {
        FlagsContentView()
    }
}
