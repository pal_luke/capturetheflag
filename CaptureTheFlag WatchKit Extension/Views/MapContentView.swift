//
//  ContentView.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 15/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import SwiftUI
import MapKit
import WatchKit

struct MapContentView: WKInterfaceObjectRepresentable {
    
    var myMapView: WKInterfaceMap!
    
    func makeWKInterfaceObject(context: WKInterfaceObjectRepresentableContext<MapContentView>) -> WKInterfaceMap {
        return WKInterfaceMap()
    }
    
    func updateWKInterfaceObject(_ myMap: WKInterfaceMap, context: WKInterfaceObjectRepresentableContext<MapContentView>) {
        
        
        let location = CLLocationCoordinate2D(latitude: 40.8365776, longitude: 14.3062209)
        let areaSpan = MKCoordinateSpan(latitudeDelta: 0.085, longitudeDelta: 0.085)
        let areaRegion = MKCoordinateRegion(center: location, span: areaSpan)
        
        // Location Manager
        let locationManager : CLLocationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        //        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        locationManager.allowsBackgroundLocationUpdates = false
        
        myMap.setRegion(areaRegion)
        if UserDefaults.standard.bool(forKey: "isMapVisible") {
            myMap.setShowsUserLocation(true)
            myMap.setShowsUserHeading(true)
            
            
            //        var annotation = MKPointAnnotation()
            //        annotation.coordinate = location
            //        annotation.title = "Big Ben"
            //        annotation.subtitle = "London"
            myMap.addAnnotation(CLLocationCoordinate2D(latitude: 40.8365796, longitude: 14.3262209), with: UIImage(named: "earthPin"), centerOffset: CGPoint(x: 0, y: 0))
            myMap.addAnnotation(CLLocationCoordinate2D(latitude: 40.8505796, longitude: 14.3562209), with: UIImage(named: "airPin"), centerOffset: CGPoint(x: 0, y: 0))
            myMap.addAnnotation(CLLocationCoordinate2D(latitude: 40.8665796, longitude: 14.3262209), with: UIImage(named: "waterPin"), centerOffset: CGPoint(x: 0, y: 0))
            myMap.addAnnotation(CLLocationCoordinate2D(latitude: 40.8165796, longitude: 14.3562209), with: UIImage(named: "firePin"), centerOffset: CGPoint(x: 0, y: 0))
            myMap.addAnnotation(CLLocationCoordinate2D(latitude: 40.8565750, longitude: 14.3062209), with: UIImage(named: "firePin"), centerOffset: CGPoint(x: 0, y: 0))
        } else {
            myMap.removeAllAnnotations()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MapContentView()
    }
}
