//
//  HostingController.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 15/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class MapHostingController: WKHostingController<MapContentView> {
    override var body: MapContentView {
        return MapContentView()
    }
}
