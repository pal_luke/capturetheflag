//
//  NotificationView.swift
//  CaptureTheFlag WatchKit Extension
//
//  Created by Luca Palmese on 15/01/2020.
//  Copyright © 2020 Luca Palmese. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
